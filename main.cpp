#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/wdt.h>

extern "C"
{
#include "usbdrv.h"
}

#define F_CPU 12000000L
#include <util/delay.h>

#include "NRF24L01Config.h"
#include "../common/NRF24L01P.h"

static uchar replyBuf[4];

#define USB_LED_OFF         0
#define USB_LED_ON          1
#define GET_REGISTER_VALUE  2


void UART_init()
{
    // Formula for noraml asynchronous mode
    // UBRR = F_OSC/(16*BAUD)  -  1

    // F_CPU 12000000
    // BAUD 19200
    // UBRR 38.0625

    UBRRL = 38;  // set prescaler for baud rate
    UCSRB |= 0x08; // enable transmitter
}

void UART_Transmit( unsigned char data )
{
    while ( ! ( UCSRA &  ( 1 << UDRE) ) );
    UDR = data;
}

class SPI
{
public:
    SPI() {
        // Set MOSI and SCK pin as output
        DDRB |= (1<<5)|(1<<3);

        // Master
        // double speed
        // SPI running at F_CPU/2 ( 6MHz)
        // Enable SPE
        SPCR = (1<<SPE)|(1<<MSTR)|(1<<SPI2X);
    }

    unsigned char writeRead( unsigned char data ) {
        SPDR = data;
        while(!(SPSR & (1<<SPIF) ));
        return(SPDR);
    }

    SPI( const SPI& other) = delete;
    SPI& operator =( const SPI& other) = delete;
    SPI& operator =( const SPI&& other) = delete;
    SPI( const SPI&& other) = delete;
};

NRF24L01P<SPI>* theNrf;

usbMsgLen_t usbFunctionSetup(uchar data[8])
{
    usbRequest_t *rq = reinterpret_cast<usbRequest_t*>(data); // cast data to correct type

    switch(rq->bRequest)
    { // custom command is in the bRequest field
    case USB_LED_ON:
        PORTC |= (1 << 5); // turn LED on
        return 0;
    case USB_LED_OFF:
        PORTC &= ~(1 << 5); // turn LED off
        return 0;
    case GET_REGISTER_VALUE:
        uint8_t reg = static_cast<unsigned char>(rq->wValue.word);
        replyBuf[0] = theNrf->getRegister(reg);
        usbMsgPtr = (usbMsgPtr_t)replyBuf;
        return 1;
    }

    return 0; // should not get here
}

int main() {

    UART_init();
    // NRF24l01+ output configuration
    // enable CSN(PB2) as output
    DDRB |= (1<<2);

    // enable CE(PD5) as output
    DDRD |= (1<<5);

    SPI spi;
    NRF24L01P<SPI> theRf(&spi);
    theNrf = &theRf;

    DDRC |= (1<<5);
    wdt_enable(WDTO_1S); // enable 1s watchdog timer

    usbInit();

    usbDeviceDisconnect(); // enforce re-enumeration
    for(uint8_t i = 0; i<250; i++) { // wait 500 ms
        wdt_reset(); // keep the watchdog happy
        _delay_ms(2);
    }
    usbDeviceConnect();

    sei(); // Enable interrupts after re-enumeration

    while(1) {
        wdt_reset(); // keep the watchdog happy
        usbPoll();
    }

    return 0;
}
